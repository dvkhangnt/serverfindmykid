const express = require('express');
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client('393095082194-dqijjr4vfj670jpuafqskg5pm8lbohlj.apps.googleusercontent.com');
const moment = require('moment');
const momentTz = require('moment-timezone');

const kidModel = require('../models/kid.model');
const historyModel = require('../models/history.model');
const router = express.Router();
const timeHistory = 0 * 60 * 1000; //cach 0p luu toa do 1 lan


router.get('/test-time', async(req, res)=>{
    var currentDate = new Date();
    let hourDiff = currentDate.getDay();
    console.log('currentDate: ', currentDate);
    console.log('month: ', hourDiff);
    currentDate = JSON.stringify(currentDate);
    currentDate = new Date(JSON.parse(currentDate));
    console.log('currentDate: ', currentDate);
    console.log('day: ',currentDate.getDate() ,' - month: ', currentDate.getMonth() + 1,'')
    var aestTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Ho_Chi_Minh"});
    console.log('Asia time: '+ (new Date(aestTime)).toISOString());
});

//Lưu tọa độ của con (nhận tọa độ, id của con) truy xuất bảng kid bằng id, cập nhật toạ độ vào trường location
 router.put('/location', async(req, res)=>{
    if( req.body.id               === undefined || 
        req.body.id.length        === 0         || 
        req.body.latitude         === undefined ||
        req.body.latitude.length  === 0         ||
        req.body.longitude        === undefined ||
        req.body.longitude.length === 0         ||
        req.body.locationName     === undefined ||
        req.body.locationName.length === 0
    ){
        return res.status(400).json({status: -1});
    }

    
    var body = req.body;
    console.log('body', body);
    const id = String(body.id) || -1;
    if(id === -1){
      return res.status(204).json({status: -1});
    }

    const kid = await kidModel.find(id);
    var obj = Object.keys(kid).length;
    if(obj < 1)
    {
        return res.status(204).json({status: -1});
    }

    //change timezone
    // var aestTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Ho_Chi_Minh"});
    // var currentTime = (new Date(aestTime)).toISOString();
    // console.log('AEST time: '+ currentTime);
    let timet = moment().format('YYYY-MM-DD HH:mm:ss');
    let currentTime = momentTz(timet).tz('Asia/Ho_Chi_Minh').format('YYYY-MM-DD HH:mm:ss');

    console.log('current time: ', currentTime);
    const latitude  = body.latitude;
    const longitude = body.longitude;
    const locationName = body.locationName;
    var l = {
        'latitude' : latitude,
        'longitude': longitude,
        'locationName': locationName,
        'currentTime': currentTime
    };
    await kidModel.update(id, l);
    console.log('update location done!');

    let time = await historyModel.singleTimeById(id);
    console.log('time length: ', time.length);
    if(time.length === 0){

        console.log('crtime: ', currentTime);
        let entity = ({
            id: id,
            time: currentTime,
            latitude: latitude,
            longitude: longitude,
            locationName: locationName
        });
        console.log('time: ', currentTime);
        await historyModel.add(entity);
        console.log('completed update history');
    }
    else {

        let t = momentTz(time[0].time).tz('Asia/Ho_Chi_Minh').format('YYYY-MM-DD HH:mm:ss');
        let delayTime = moment(currentTime).diff(moment(t));
        console.log('timeHistory: ', timeHistory);
        //if(delayTime >= timeHistory){
        console.log('timeHistory into: ', timeHistory);

            let entity = ({
                id: id,
                time: currentTime,
                latitude: latitude,
                longitude: longitude,
                locationName: locationName
            });
            await historyModel.add(entity);
            console.log('completed update history');
       // }
    }
    res.json({status: 1});
 });

//lấy vị trí của kid theo id
 router.post('/location-by-id', async(req, res)=>{
    if(req.body.id === undefined || req.body.id.length === 0){
        return res.status(400).send('bad request!').end();
    }

    const kid = await kidModel.find(req.body.id);
    if(kid.length === 0) {
        return res.status(204).send('id incorrect!').end();
    }

    let location = {
        'latitude': kid[0].latitude,
        'longitude': kid[0].longitude,
        'currentTime': kid[0].currentTime
    }

    console.log('location: ', location);
    res.status(200).json(location);
 });

//danh sách kid
router.get('/', async(req, res)=>{
    const list = await kidModel.all();
    console.log('direct home');
	res.json(list);
});

//dang ki tai khoan cho kid bang google authentication
router.post('/auth/google', async(req, res)=>{
    if(req.body.idToken === '' || req.body.idToken === undefined){
        return res.status(204).send('bad request!');
    }


    const ticket = await client.verifyIdToken({
        idToken: req.body.idToken,
        audience: '393095082194-dqijjr4vfj670jpuafqskg5pm8lbohlj.apps.googleusercontent.com' // Specify the CLIENT_ID of the app that accesses the backend
    });
    const payload = ticket.getPayload();
    console.log('payload: ', payload);

    const dt = await kidModel.find(payload['sub']);
    if(dt === undefined || dt.length === 0){
        const listCode = await kidModel.allCode();
        let code = generateCode(listCode);
        var entity = {
            'id'       : payload['sub'],
            'code'     : code,
            'name'     : payload['name'],
            'email'    : payload['email'],
            'avatar'   : payload['picture'],
            'phone'    : '00'
        };

        console.log('entity', entity)
        
        await kidModel.add(entity);
        console.log('done');

        return res.status(200).json(entity);
    }
    else {
        console.log('account is exist!', dt);
        var rs = {
            'id'    : dt[0].id,
            'code'  : dt[0].code,
            'name'  : dt[0].name,
            'email' : dt[0].email,
            'avatar': dt[0].avatar,
            'phone' : dt[0].phone
        }
        return res.status(200).json(rs);
    }

});

//update phone number for kid
router.put('/phone', async(req, res)=>{
    if(req.body.id === undefined || req.body.id.length === 0 || req.body.phone === undefined || req.body.phone.length === 0){
        return res.status(400).send('request error!').end();
    }

    const kid = await kidModel.find(req.body.id);
    if(kid.length === undefined || kid.length === 0){
        return res.status(204).send('id incorrect!').end();
    }

    var p = {'phone': req.body.phone};
    await kidModel.update(req.body.id, p);
    console.log('update phone kid completed!');
    res.json(p);

});

//generate code for kid
router.put('/generate-code', async(req, res)=>{
    if(req.body.id === undefined || req.body.id.length === 0){
        return res.status(400).send('request error!').end();
    }
    const kid = await kidModel.find(req.body.id);
    if(kid.length === undefined || kid.length === 0){
        return res.status(204).send('id incorrect!').end();
    }

    const listCode = await kidModel.allCode();
    let code = generateCode(listCode);
    var entity = {
        'code': code
    }

    await kidModel.update(req.body.id, entity);

    res.status(200).json(entity);
});

//trả về id max trong list danh sách
getMaxId = (list)=>{
    let max = -1;
    list.forEach(e =>{
        if(e.id > max){
            max = e.id;
        }
    })
    return max;
}

// tạo code, tìm trong list code đã tồn tại chưa, nếu tồn tại thì tạo lại code mới, nếu chưa tồn tại thì trả về code
generateCode = (list)=>{
    var code = '';
    var flag = true;
    while(flag){
        flag = false;
        code = Math.floor(Math.random()*(0xfffff-0x10000) + 0x10000).toString(16);
        list.forEach(e=>{
            if(e.code == code){
                flag = true;
            }
        })
    }
    return code;
}


module.exports = router;