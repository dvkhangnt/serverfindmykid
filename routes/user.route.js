const express = require('express');
const userModel = require('../models/user.model');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const GooglePlusTokenStrategy = require('passport-google-plus-token');
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client('474141562918-otbvrvadrfbro5dhguii68jqfai3j42v.apps.googleusercontent.com');

const router = express.Router();

passport.serializeUser(function (user, done) {
    done(null, user.id);
});
// used to deserialize the user
passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

router.post('/auth/google', async(req, res)=>{
  if(req.body.idToken === '' || req.body.idToken === undefined){
    return res.status(204).send('bad request!');
  }
  const ticket = await client.verifyIdToken({
    idToken: req.body.idToken,
    audience: '474141562918-otbvrvadrfbro5dhguii68jqfai3j42v.apps.googleusercontent.com' // Specify the CLIENT_ID of the app that accesses the backend
  });
  console.log('a====================================');
  const payload = ticket.getPayload();

  const list = await userModel.all();
  var dt = list.find((value, index, arr)=>{
    return value.id === payload['sub'];
  });

  if(dt === undefined){
    var entity = {
      'id'    : payload['sub'],
      'name'  : payload['name'],
      'email' : payload['email'],
      'avatar': payload['picture'],
      'phone' : '00'
    };

    console.log('entity', entity)
    
    await userModel.add(entity);
    console.log('done');

    return res.status(200).json(entity);
  }
  else {
    console.log('account is exist!', dt);
    return res.status(200).json(dt);
  }

  
})

 router.put('/phone', async(req, res)=>{
    var body = req.body;
    console.log('body', body);
    const id = String(body.id) || -1;
    if(id === -1){
      return res.status(204).end();
    }
    const phone = String(body.phone);
    var p = {'phone': body.phone};
    await userModel.update(id, p);
    console.log('done!', body.phone)
    res.status(200).json(p);
 })

router.get('/', async(req, res)=>{
	const list = await userModel.all();
  console.log('direct home');
	res.json(list);
});



module.exports = router;