const express = require('express');
const moment = require('moment');
const momentTz = require('moment-timezone');
const historyModel = require('../models/history.model');

const router = express.Router();


// lưu lại tọa độ đầu tiên mỗi khi có khung giờ chẵn 
 router.put('/update-history', async(req, res)=>{
    var body = req.body;
    console.log('body', body);
    const id = String(body.id) || -1;
    if(id === -1){
      return res.status(204).end();
    }
    const kid = await historyModel.find(id);
    var obj = Object.keys(kid).length;
    if(obj < 1)
    {
        return res.status(404).send('not found');
    } 
    // thời gian ở đây do client (Date.now) gửi qua nó là một con số được tính theo giây
    // lưu kiểu varchar(255) dưới db, a có thử lưu trử dạng Datetime
    // nhưng khi test bằng postman anh đưa vào một chuỗi vd: 2019-09-09T8:00:00Z
    // thì khi thực hiện update nó hiểu ngầm 2019-09-09 là một phép tính cho ra kết qua và không thục hiện được
    // kết quả đó là 2001T8:00:00Z nên ko thực hiện đc
    const time = body.time;
    const location = body.location;
    await historyModel.update(id, Date.now(), location);
    console.log('done!')
    res.send('ok');
 })

router.get('/', async(req, res)=>{
    const list = await historyModel.all();
    console.log('list: ', list);
    let time = list[4].time;
    let m = new Date(time);
    console.log('time after: ', m.getMonth() + 1);
    console.log('direct home');
	res.json(list);
});

router.post('/',async(req, res)=>{
    let time = new Date();
    var kid = {
        'id': req.body.id,
        'time': time,
        'location': req.body.location
    }
    await historyModel.add(kid);
    res.status(200).send('done');
});

router.post('/location-by-time', async(req, res)=>{
    let id = req.body.id;
    let startDay = req.body.startDay;
    let endDay = req.body.endDay;

    if(id === undefined || 
        id.length === 0 || 
        startDay === undefined || 
        startDay.length === 0
      ){
        return res.json({
            status: -1
        });
    }

    if(endDay === undefined || endDay.length === 0){ // lấy lịch sử trong ngày
        let time = moment(startDay).format('YYYY-MM-DD HH:mm:ss');
        let time1 = moment(moment(time).add(1, 'd'));
        let time2 = moment(moment(time1).add(-1, 's')).format('YYYY-MM-DD HH:mm:ss');
        console.log('time1: ', time);
        console.log('time2: ', time2);
        let rows = await historyModel.rowsByIdBetween(id, time, time2);
        if(rows.length === 0){
            return res.json({
                status: -1
            });
        }

        // rows.forEach(e=>{
        //     e.time = momentTz(e.time).tz('Asia/Ho_Chi_Minh').format('YYYY-MM-DD HH:mm:ss');
        // })

        res.json({
            status: 1,
            history: rows
        });
    }
    else { // lấy lịch sử trong khoảng
        let time0 = moment(startDay).format('YYYY-MM-DD HH:mm:ss');
        let time = moment(endDay).format('YYYY-MM-DD HH:mm:ss');
        let time1 = moment(moment(time).add(1, 'd'));
        let time2 = moment(moment(time1).add(-1, 's')).format('YYYY-MM-DD HH:mm:ss');
        console.log('time1: ', time);
        console.log('time2: ', time2);
        let rows = await historyModel.rowsByIdBetween(id, time0, time2);
        if(rows.length === 0){
            return res.json({
                status: -1
            });
        }

        // rows.forEach(e=>{
        //     e.time = momentTz(e.time).tz('Asia/Ho_Chi_Minh').format('YYYY-MM-DD HH:mm:ss');
        // })
        
        res.json({
            status: 1,
            history: rows
        });
    }
});

router.post('/location', async(req, res)=>{
    let { id } = req.body;
    let rows = await historyModel.rowsById(id);
    res.json(rows);
});

module.exports = router;