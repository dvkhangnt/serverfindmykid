const express = require('express');

const kidsModel = require('../models/kids.model');
const userModel = require('../models/user.model');
const kidModel = require('../models/kid.model');

const router = express.Router();


//Trả về danh sách kid của một user
 router.post('/list', async(req, res)=>{
    var body = req.body;
    console.log('body', body);
    const id = String(body.idUser) || -1;
    if(id === -1){
      return res.status(400).end();
    }
    var entity = {'idUser': id};
    var kids = await kidsModel.listKids(id);
    var obj = Object.keys(kids).length;
    if(obj < 1)
    {
        return res.status(204).send('not found');
    }
    let ret = ({
        listChildren: kids
    });
    console.log('ret: ', ret);
    res.json(ret);
 })


 router.get('/', async(req, res)=>{
    const list = await kidsModel.all();
    console.log('direct home');
	res.json(list);
});

// cha nhap code cua con
router.post('/',async(req, res)=>{
    if( req.body.idUser        === undefined || 
        req.body.idUser.length === 0         || 
        req.body.code          === undefined ||
        req.body.code.length   === 0
    ){
        return res.status(400).json({status: -1});
    }

    //kiem tra id user co ton tai hay khong
    const user = await userModel.find(req.body.idUser);
    if(user === undefined || user.length === 0){
        return res.json({status: -1});
    }

    //kiem tra code co ton tai hay khong
    var entity = {'code': req.body.code};
    const kid = await kidModel.findEntity(entity);
    if(kid === undefined || kid.length === 0){
        return res.json({status: -1});
    }
    const idKid = kid[0].id;
    console.log('id: ', idKid);

    //kiem tra cha vs con da duoc ket noi chua
    entity = {
        'idUser': req.body.idUser
    };
    console.log('entity: ', entity);
    const kidsItemp = await kidsModel.findEntity(entity);
    var flag = false;
    kidsItemp.forEach(e=>{
        if(e.idKid === idKid){
            flag = true;
            return false;
        }
    });
    if(flag){
        return res.json({status: 2}).end();
    }

    //connect kid with parent
    entity = {
        'idUser': req.body.idUser,
        'idKid': idKid
    }
    await kidsModel.add(entity);
    console.log('connected!: ', entity);
    const ret = ({
        'status': 1
    })
    res.status(201).json(ret);
});


module.exports = router;