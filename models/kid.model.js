const db = require('../utils/db');

module.exports = {
    all: _=> db.load('SELECT * FROM KID'),
    allCode: _=> db.load('SELECT code FROM KID'),
	add: entity =>{
		return db.add(entity, 'KID')
	},
	update: (id, entity)=>{
		return db.update(id, entity, 'KID')
    },
    find: (id)=>{
        return db.find(id,'KID')
    },
    findEntity: (entity)=>{
    	return db.findEntity(entity, 'KID')
    }
}