const db = require('../utils/db');

module.exports = {
    all: _=> db.load('SELECT * FROM HISTORY'),
	add: entity =>{
		return db.add(entity, 'HISTORY')
	},
	update: (id, time, location)=>{
		return db.updateforhist(id, 'HISTORY', time, location)
    },
    find:(id)=>{
        return db.find(id,'HISTORY')
    },
    singleTimeById: async (id)=>{
    	const sql = `select time from HISTORY where id = ${id} ORDER BY time DESC limit 1`;
    	const rows = await db.load(sql);
    	return rows;
    },
    rowsById: async (id) =>{
        const sql = `select * from HISTORY where id = ${id}`;
        const rows = await db.load(sql);
        return rows;
    },
    rowsByIdBetween: async(id, time1, time2) =>{
        const sql = `select time, latitude, longitude, locationName from HISTORY where time BETWEEN '${time1}' AND '${time2}' AND id = ${id}`;
        console.log('sql: ', sql);
        const rows = await db.load(sql);
        return rows;
    }

}