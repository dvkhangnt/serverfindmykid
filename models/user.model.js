const db = require('../utils/db');

module.exports = {
	all: _=> db.load('SELECT * FROM USERS'),
	add: entity =>{
		return db.add(entity, 'USERS')
	},
	update: (id, entity)=>{
		return db.update(id, entity, 'USERS')
	},
	find: (id)=>{
		return db.find(id, 'USERS')
	}
}