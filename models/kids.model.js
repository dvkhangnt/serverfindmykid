const db = require('../utils/db');

module.exports = {
    all: _=> db.load('SELECT * FROM KIDS'),
	add: entity =>{
		return db.add(entity, 'KIDS')
	},
    find:(idUser)=>{
        return db.findkid(idUser)
    },
    listKid: _=>db.load('SELECT * FROM KID'),
    listUser: _=>db.load('SELECT * FROM USERS'),
    findEntity: (entity)=>{
    	return db.findEntity(entity, 'KIDS')
    },
    listKids: id=>{
        return db.listKids(id, 'KIDS', 'KID')
    }

}