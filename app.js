const express = require('express');
const passport = require('passport');
const bodyparser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
require('express-async-errors');

const app = express();

app.use(morgan('dev'));
app.use(cors());
app.use(bodyparser.json());
app.use(express.json());

app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res)=>{
	res.json({
		msg: 'Hello from nodejs Server port 3000'
	})
})

app.use('/api/users', require('./routes/user.route'));
app.use('/api/kid', require('./routes/kid.route'));
app.use('/api/history', require('./routes/history.route'));
app.use('/api/kids', require('./routes/kids.route'))

app.use((req, res, next)=>{
	res.status(404).send('NOT FOUND');	
})

app.use(function(err, req, res, next){
	console.log(err.stack);
	res.status(500).send('View error log on console');
})

//const PORT = 3000;
app.listen(process.env.PORT || 3000, _=>{
	console.log(`API is running`);
})