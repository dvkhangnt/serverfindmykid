const mysql = require('mysql');
const { promisify } = require('util');

const pool = mysql.createPool({
	connectionLimit: 1000,
	connectTimeout: 60 * 60 * 1000,
	acquireTimeout: 60 * 60 * 1000,
	timeout: 60 * 60 * 1000,
	// host: 'db4free.net',
	// user: 'dvkhangnt', 
	// password: '123456789',
	// database: 'protectkid',
	host: 'sql12.freemysqlhosting.net',
	user: 'sql12361366',
	password: 'XzATiCeiUv',
	database: 'sql12361366',
	multipleStatements: true
});

pool.getConnection(function (err, connection) {
	if (err) {
		console.log('failure connect database!');
		throw err;
	} // not connected!
	else console.log('connected database!');
});

const pool_query = promisify(pool.query).bind(pool);

module.exports = {
	load: sql => pool_query(sql),
	add: (entity, tableName) => pool_query(`insert into ${tableName} set ?`, entity),
	update: (id, entity, tableName) => pool_query(`update ${tableName} set ? where id = ${id}`, entity),
	find: (id, tablename) => pool_query(`select * from ${tablename} where id = ${id}`),
	updateforhist: (id, tablename, time, location) => pool_query(`update ${tablename} set time = ${time}, location = ${location} where id = ${id}`),
	findkid: (idUser) => pool_query(`select id, location from KID, KIDS where idUser = ${idUser} and KID.code = KIDS.code`),
	findEntity: (entity, tablename) => pool_query(`select * from ${tablename} where ?`, entity),
	listKids: (id, tableName1, tableName2) => pool_query(`select ${tableName2}.* from ${tableName1}, ${tableName2} where ${tableName1}.idKid = ${tableName2}.id and ${tableName1}.idUser = '${id}' `),
};